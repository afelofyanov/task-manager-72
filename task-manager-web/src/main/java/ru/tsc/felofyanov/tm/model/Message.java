package ru.tsc.felofyanov.tm.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

    private String value;

    public Message(String value) {
        this.value = value;
    }
}
