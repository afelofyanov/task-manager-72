package ru.tsc.felofyanov.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.felofyanov.tm.exception.AccessException;
import ru.tsc.felofyanov.tm.model.CustomUser;

public class UserUtil {

    public static String getUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessException();
        if (!(principal instanceof CustomUser)) throw new AccessException();
        final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }
}
