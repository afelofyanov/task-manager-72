package ru.tsc.felofyanov.tm.exception;

public final class UserIdEmptyException extends AbstractException {
    public UserIdEmptyException() {
        super("Error! User Id is empty...");
    }
}
