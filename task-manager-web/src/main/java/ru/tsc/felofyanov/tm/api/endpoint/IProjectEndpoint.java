package ru.tsc.felofyanov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project")
            @RequestBody Project project
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "projects")
            @RequestBody List<Project> projects
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findAll")
    List<Project> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    Project save(
            @WebParam(name = "project")
            @RequestBody Project project
    );

    @WebMethod
    @PostMapping("/saveAll")
    List<Project> saveAll(
            @WebParam(name = "projects")
            @RequestBody List<Project> projects
    );
}
