package ru.tsc.felofyanov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsc.felofyanov.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_role")
public class Role {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USUAL;

    @ManyToOne
    private User user;

    @Override
    public String toString() {
        return roleType.name();
    }
}
