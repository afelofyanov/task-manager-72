package ru.tsc.felofyanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.felofyanov.tm.service.TaskService;
import ru.tsc.felofyanov.tm.util.UserUtil;

@Controller
public class TasksController {

    @Autowired
    private TaskService service;

    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("task-list", "tasks", service.findAll(UserUtil.getUserId()));
    }
}
