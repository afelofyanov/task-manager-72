package ru.tsc.felofyanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.CustomUser;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.service.ProjectService;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService service;

    @GetMapping("/project/create")
    public String create(@AuthenticationPrincipal CustomUser user) {
        service.create(user.getUserId());
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@AuthenticationPrincipal CustomUser user, @PathVariable("id") String id) {
        service.removeByByUserIdAndId(user.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    public String edit(@AuthenticationPrincipal CustomUser user, @ModelAttribute("project") Project project, BindingResult result) {
        service.save(user.getUserId(), project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@AuthenticationPrincipal CustomUser user, @PathVariable("id") String id) {
        final Project project = service.findFirstByUserIdAndId(user.getUserId(), id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    public Status[] getStatuses() {
        return Status.values();
    }
}
