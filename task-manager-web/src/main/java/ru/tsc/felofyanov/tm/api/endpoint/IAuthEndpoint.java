package ru.tsc.felofyanov.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tsc.felofyanov.tm.model.Result;
import ru.tsc.felofyanov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpoint {

    @WebMethod
    @PostMapping("/login")
    Result login(
            @WebParam(name = "login") String login,
            @WebParam(name = "password") String password
    );

    @WebMethod
    @PostMapping("/logout")
    Result logout();

    @WebMethod
    @GetMapping("/profile")
    User profile();
}
