package ru.tsc.felofyanov.tm.exception;

public final class LoginEmptyException extends AbstractException {
    public LoginEmptyException() {
        super("Error! Login is empty...");
    }
}
