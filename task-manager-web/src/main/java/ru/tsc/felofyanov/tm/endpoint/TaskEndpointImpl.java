package ru.tsc.felofyanov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.felofyanov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.service.TaskService;
import ru.tsc.felofyanov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.felofyanov.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @Autowired
    private TaskService service;

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return service.count(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task")
            @RequestBody Task task
    ) {
        service.removeByUserIdAndEntity(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "tasks")
            @RequestBody List<Task> tasks
    ) {
        service.remove(UserUtil.getUserId(), tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        service.clear(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        service.removeByByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        return service.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return service.findAll(UserUtil.getUserId()).stream().collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) {
        return service.findFirstByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(
            @WebParam(name = "task")
            @RequestBody Task task
    ) {
        return service.save(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PostMapping("/saveAll")
    public List<Task> saveAll(
            @WebParam(name = "tasks")
            @RequestBody List<Task> tasks
    ) {
        return service.save(UserUtil.getUserId(), tasks);
    }
}
