package ru.tsc.felofyanov.tm.exception;

public final class RoleEmptyException extends AbstractException {
    public RoleEmptyException() {
        super("Error! Role is empty...");
    }
}
