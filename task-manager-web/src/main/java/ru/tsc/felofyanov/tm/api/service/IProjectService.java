package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @Transactional
    void create(@Nullable String userId);

    @Transactional
    Project save(@Nullable String userId, @Nullable Project project);

    @Transactional
    List<Project> save(@Nullable String userId, @Nullable List<Project> projects);

    @Transactional
    List<Project> findAll(@Nullable String userId);

    @Transactional
    Project findFirstByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Transactional
    void removeByByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Transactional
    void removeByUserIdAndEntity(@Nullable String userId, @Nullable Project project);

    @Transactional
    void remove(@Nullable String userId, @Nullable List<Project> projects);

    @Transactional
    boolean existsById(@Nullable String userId, @Nullable String id);

    @Transactional
    void clear(@Nullable String userId);

    @Transactional
    long count(@Nullable String userId);
}
