package ru.tsc.felofyanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.DataLoadXmlJaxbRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;

@Component
public final class DataLoadXmlJaxbListener extends AbstractDataListener {

    @NotNull
    @Override
    public String getName() {
        return "data-load-xml-jaxb";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from xml file(JAXB)";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataLoadXmlJaxbListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        getDomainEndpoint().loadDataXmlJaxb(new DataLoadXmlJaxbRequest(getToken()));
    }
}
