package ru.tsc.felofyanov.tm.listener.taskproject;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.ProjectUnbindTaskByIdRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class UnbindTaskFromProjectListener extends AbstractTaskProjectListener {

    @NotNull
    @Override
    public String getName() {
        return "unbind-task-to-project";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unbind task from project.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@unbindTaskFromProjectListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UNBIND TASK FROM PROJECT]");

        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final ProjectUnbindTaskByIdRequest request =
                new ProjectUnbindTaskByIdRequest(getToken(), projectId, taskId);
        getProjectTaskEndpoint().unbindTaskFromProjectId(request);
    }
}
