package ru.tsc.felofyanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.DataLoadBackupRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;

@Component
public final class DataLoadBackupListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "load-backup";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load backup from file";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataLoadBackupListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        getDomainEndpoint().loadDataBackup(new DataLoadBackupRequest(getToken()));
    }
}
