package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataLoadXmlFasterXmlRequest extends AbstractUserRequest {

    public DataLoadXmlFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
