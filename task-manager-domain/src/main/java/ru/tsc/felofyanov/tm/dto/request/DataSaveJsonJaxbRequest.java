package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataSaveJsonJaxbRequest extends AbstractUserRequest {

    public DataSaveJsonJaxbRequest(@Nullable String token) {
        super(token);
    }
}
