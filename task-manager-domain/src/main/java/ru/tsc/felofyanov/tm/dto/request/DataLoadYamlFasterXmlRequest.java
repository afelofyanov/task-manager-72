package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataLoadYamlFasterXmlRequest extends AbstractUserRequest {

    public DataLoadYamlFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
