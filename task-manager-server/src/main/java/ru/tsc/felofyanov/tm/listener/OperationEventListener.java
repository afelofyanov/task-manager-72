package ru.tsc.felofyanov.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.service.ISenderService;
import ru.tsc.felofyanov.tm.dto.OperationEvent;
import ru.tsc.felofyanov.tm.service.SenderService;

import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class OperationEventListener implements Consumer<OperationEvent> {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final ISenderService service = new SenderService();

    @NotNull
    private final ExecutorService es = Executors.newCachedThreadPool();

    @Override
    @SneakyThrows
    public void accept(@NotNull final OperationEvent operationEvent) {
        final Class entityClass = operationEvent.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            final Annotation annotation = entityClass.getAnnotation(Table.class);
            final Table table = (Table) annotation;
            operationEvent.setTable(table.name());
        }
        @NotNull final String json = objectWriter.writeValueAsString(operationEvent);
        es.submit(() -> service.send(json));
    }
}
